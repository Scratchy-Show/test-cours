# Premier cours de **GIT**

## Repository
---
1. Clic droit dans le dossier cible

2. Option : **Git Bash Here**

3. Dans la console noter la commande :
```git
git clone
```

4. Insérer l'URL du projet avec la commande 
```git
Shit + Insert
```

5. Faire les modifications locales

6. Ajouter les modifications avec un *commit*

7. Commit tous les changements
```git
git commit -m "message"
```

8. Mettre à jour le repo du serveur
```git
git push
```

9. Mettre à jour le repo local avec les données du repo du serveur
```git
git pull
```